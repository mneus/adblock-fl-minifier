# Internship project : FL-Minifier

## A Tool To Minify and Unify AdBlocker's Filter Lists

## Tasks

### Completed :

- Build a Chrome extension to run adblocking tests
  - Inserts testing elements into visited pages and checks their visibility
  - Displays detected lists in popup (crude UI)
  - Shows a page of the history of changes in detected lists

- Create a database scheme to store users' filter list data
  [scheme representation place holder]

- Build an API to interface with the database and the backend
  - GET testing elements
  - POST tests results
  - GET user statistics
  - GET user current detected lists

- Build a backend pipeline to run tests
  
- Build a pipeline to create tests from filter lists data
  - Needs polishing
  
### To Do

- Create a testing scheme for filter lists versions
- Polish test creation pipeline
- Polish history page's UI


# Links
[Github](https://github.com/melurne/Stage_FL-Minifier)
[Gitlab-Inria](https://gitlab.inria.fr/mneus/adblock-fl-minifier)