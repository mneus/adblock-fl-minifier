google.com##.gb_vd[aria-label*="Google Play"][aria-label*=Chrome]
google.*##.gb_g.gb_ba:has(.gb_Ad)
chrome.google.com##.Yb
futureordering.com,bestill.egon.no###unsupported
##.chrome-footer:not(html, body)
##.chrome-banner:not(html, body)
##.epb-container.f-information.c-uhfh-alert
bing.com#?##b_pole:-abp-contains(Microsoft Edge)
bing.com##.bnp_btc
bing.com##.bnp_fly_hp
bing.com##[data-flyout=anaheim]
microsoft.com,windows.com###epb
microsoft.com,windows.com##bannerAboveUHF
microsoft.com,windows.com##banner-above-uhf
msn.com###irisbannerph
msn.com##.articlebody > div[style="width:100%;border:1px solid #CCC;padding:10px;"]
outlook.live.com##div[class*=" body-"] > div[class] > div[class]:not([role]) > div:first-of-type:not([class])
addons.opera.com###overlay
||opera.com/*/downloadassets/sticky-bar-auto|$subdocument
addons.mozilla.org##.DownloadFirefoxButton
send.firefox.com##send-promo
###dashboard_browser_nag
softonic.com##[src*="/avastsecurebrowser/"]
###download-recommended-app
||united-infos.net^$third-party
archive.is,archive.today,archive.vn,archive.fo,archive.md,archive.li,archive.ph,archivecaslytosk.onion,archiveiya74codqgiixo33q62qlrqtkgmcitqx5u2oeqnmn5bpcbiyd.onion,btdig.com##+js(acis, document.cookie, document.location.href)
asrock.com###Manual > .Notice
/getAdobeReader.$image
jal.co.jp#?#.mgt20:-abp-contains(is necessary to view or print PDF)
jal.com#?#body > font:-abp-contains(have Adobe Reader)
yadvashem.org#?#.text_and_menu_block > p:-abp-contains(need to download Adobe Acrobat)
yadvashem.org#?#p:-abp-contains(start your Acrobat Reader)
gigabyte.com#?#div.target-container.desc:-abp-contains(PDF files with Acrobat )
/get_adobe_reader.$image
supermicro.com#?#.boxHome.textA:-abp-has([href*="products/acrobat/"])
@@||supermicro.com$elemhide,badfilter
signform.no#?##signformheader + div > p:-abp-contains(Adobe Reader)
cia.gov,ciadotgov4sjwlzihbbgxnqg3xiyrg7so2r2o3lt5wz5ypk4sxyjstad.onion#?#.info-panel:-abp-contains(Adobe® Reader®)
/images/acrobat.gif|
deezer.com#?#.page-alerts:-abp-has(a[href$="/download"])
fileinfo.com##.fv
fileinfo.com#?#.top:-abp-has(a[href*="/windows_file_viewer"])
